from CO2Meter import *
import time
import json
import os

sensor = CO2Meter("/dev/hidraw0")
while True:
    data = sensor.get_data()
    if "co2" in data and "temperature" in data:
        co2 = data["co2"]
        temp = data["temperature"]

        print("co2: %d, temp: %d" % (co2, temp))

        if co2 > 1100:
            os.system("aplay Atone.wav")

    time.sleep(60)
