# co2-alarm: audio warnings for bad office air

co2-alarm is as the name says a simple python script to monitor the sensor output of a co2meter.

At the moment it only plays a warning sound every 60 seconds while the detected ppm is over 1100.

# Usage

Just run `python main.py`

In case you are deploying this on a Raspberry Pi (like me) with external speakers, you can use:

    amixer cset numid=3 1

to set the audio output to the 3.5mm headphone jack. <br>
You can take a look at the `run` script for an example.

